SELECT
	 1                     AS `idLine`
	,1                     AS `idPoint`
	,-10.53 + RAND() * 100 AS `XValue`
	,-15.50 + RAND() * 100 AS `YValue`

UNION ALL

SELECT
	 1                     AS `idLine`
	,2                     AS `idPoint`
	,-0.57  + RAND() * 100 AS `XValue`
	,+1.53  + RAND() * 100 AS `YValue`

UNION ALL

SELECT
	 1                     AS `idLine`
	,3                     AS `idPoint`
	,+5.51  + RAND() * 100 AS `XValue`
	,+15.55 + RAND() * 100 AS `YValue`

UNION ALL

SELECT
	 2                     AS `idLine`
	,1                     AS `idPoint`
	,-15.51 + RAND() * 100 AS `XValue`
	,-25.55 + RAND() * 100 AS `YValue`

UNION ALL

SELECT
	 2                     AS `idLine`
	,2                     AS `idPoint`
	,+25.31 + RAND() * 100 AS `XValue`
	,+35.55 + RAND() * 100 AS `YValue`
;

SELECT
	 1                     AS `idLine`
	,4                     AS `idPoint`
	,-8.44  + RAND() * 100 AS `XValue`
	,-12.05 + RAND() * 100 AS `YValue`

UNION ALL

SELECT
	 1                     AS `idLine`
	,5                     AS `idPoint`
	,-1.57  + RAND() * 100 AS `XValue`
	,+14.53 + RAND() * 100 AS `YValue`

UNION ALL

SELECT
	 1                     AS `idLine`
	,7                     AS `idPoint`
	,-12.2  + RAND() * 100 AS `XValue`
	,-12.13 + RAND() * 100 AS `YValue`
;
