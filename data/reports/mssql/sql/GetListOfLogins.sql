SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

SELECT DISTINCT
	 @@ServerName     AS [ServerName]
	,tSL.[name]       AS [LoginName]
	,tSL.[createdate] AS [CreateDateTime]
	,tSL.[hasaccess]  AS [IsHasAccess]
	,tSL.[denylogin]  AS [IsDenyLogin]
	,tSL.[language]   AS [LoginLanguage]
FROM
	[master].[dbo].[syslogins] tSL
ORDER BY
	tSL.[name]
;
