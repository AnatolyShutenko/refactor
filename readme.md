# Node Server Auditor
Node based report engine to generate reports about external servers. Currenlty "mssql" and "mysql" database servers are supported.

## Installation
Download and install node.js: https://nodejs.org/en/download/

Install "bower" globally:

```sh
$ npm install -g bower
```
Get app from repo
```sh
$ git clone https://bitbucket.org/nodeserverauditor/node-server-auditor.git node-auditor
$ cd node-auditor/
$ npm install
```
## Building an app
### Production
```sh
$ npm run build
```

### Dev Build
```sh
$ npm run devb
```
### Dev Build & Watch Source
```sh
$ npm run dev
```

## Configuration
You need to add all your "mssql" or "mysql" servers (one in each file) to the folder ~/servers/. See ~/servers/readme.md about connection format. The filename would be your connection name in application connection dropdown list.

## Run application
### Linux / OS X
```sh
$ npm start
```

### Windows
```sh
$ npm run windows
```
