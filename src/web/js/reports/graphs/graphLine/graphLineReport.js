/* global app */

/**
 * Graph Line Directive
 *
 * @requires baseGraphLink
 * @requires GraphLineComp
 */
app.directive('graphLineReport', ['baseGraphLink', 'GraphLineComp', function (link, GraphLineComp) {
	return {
		restrict: 'EA',
		templateUrl: 'directives/report/graphs/default-graph.html',
		scope: {
			reportItem: '=graphLineReport'
		},
		link: function ($scope, element, attrs) {
			link($scope, element, attrs, GraphLineComp);
		}
	};
}]);
