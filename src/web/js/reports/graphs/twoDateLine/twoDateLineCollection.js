/* global angular, app */

/**
 * Two Date Lines collection model
 *
 * @requires Data
 */
app.factory('TwoDateLineCollection', ['Data', function (Data) {
	var TwoDateLineCollection = function (Class, items) {
		this.rows = [];

		items.forEach(function (item) {
			if (!this.rows[0]) {
				this.rows[0] = [];
			}

			this.rows[0].push(new Class(item));

			// try to find property EventValueOther
			if ('EventValueOther' in item) {
				if (!this.rows[1]) {
					this.rows[1] = [];
				}

				item.EventValue = item.EventValueOther;

				this.rows[1].push(new Class(item));
			}
		}.bind(this));
	};

	angular.extend(TwoDateLineCollection.prototype, Data.prototype, {
		data: function () {
			return this.rows.map(function (row) {
				return row.map(function(item) {
					return item.data();
				});
			});
		}
	});

	return TwoDateLineCollection;
}]);
