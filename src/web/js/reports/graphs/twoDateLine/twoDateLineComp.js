/* global angular */

/**
 * Two Date Line Component
 *
 * @requires BaseGraphComp
 * @requires TwoDateLineCollection
 * Dependent from DateLineData Graph
 */
app.factory('twoDateLineComp', ['BaseGraphComp', 'DateLineData', 'TwoDateLineCollection', function (BaseGraphComp, DateLineData, TwoDateLineCollection) {
	var twoDateLineComp = function () {
		BaseGraphComp.call(this);

		angular.merge(this.options, {
			axes: {
				xaxis: {
					autoscale:   true,
					renderer:    $.jqplot.DateAxisRenderer,
					tickOptions: {
						formatString: '%Y/%#m/%#d %#H:%#M'
					}
				},
				yaxis: {
					autoscale: true
				},
				y2axis: {
					autoscale: true
				}
			},

			axesDefaults: {
				useSeriesColor: true
			},

			series: [
				{
					yaxis: 'yaxis',

					lineWidth: 2,

					markerOptions: {
						show:      false,
						style:     'square',
						lineWidth: 2
					}
				},
				{
					yaxis: 'y2axis',

					lineWidth: 2,

					markerOptions: {
						show:      false,
						style:     'square',
						lineWidth: 2
					}
				}
			]
		});
	};

	angular.extend(twoDateLineComp.prototype, BaseGraphComp.prototype, {
		updateReportData: function (data) {
			var graphLine = new TwoDateLineCollection(DateLineData, data);

			return graphLine.data();
		}
	});

	return twoDateLineComp;
}]);
