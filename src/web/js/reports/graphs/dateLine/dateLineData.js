/* global angular, app */

/**
 * Date Line Data Model
 *
 * @requires Data
 */
app.factory('DateLineData', ['Data', function (Data) {
	var GraphLineData = function (params) {
		this.row = [new Date(params.EventDateTime), params.EventValue];
	};

	angular.extend(GraphLineData.prototype, Data.prototype, {
		data: function () {
			return this.row;
		}
	});

	return GraphLineData;
}]);
